# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

#use strict;

#BEGIN { $| = 1; print "1..7\n"; }
#END { print "not ok 1\n" unless $loaded; }
use Whois;
#$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

my $failure = new Whois('Host' => '127.0.0.2');

print "not " if($failure);
print "ok 2\n";

print "not " unless($Whois::ERRORCODE);
print "ok 3\n";

my $whois = new Whois('Host' => 'whois.ripe.net');

if(ref($whois)) {
    print "ok 4\n";
}
else {
    print "not ok 4\n";
    exit;
}

if($whois->GetError()) {
    printf("Query error: %s\n", $whois->GetError());
    print "not ok 5\n";
    exit;
}
else {
    print "ok 5\n";
}

my $test = 6;

foreach my $object (qw(BAT-RIPE TIB-RIPE)) {
    if($whois->Query($object)) {
	printf("Query error: %s\n", $whois->GetError());
	print "not ok $test\n";
	exit;
    }
    else {
	print "ok $test\n";
    }
    
    $test++;
    
    my $result = $whois->GetResult();
    
    if($result) {
	print "ok $test\n";
	print "-" x 30, "\n";
	print $result;
	print "-" x 30, "\n";
    }
    else {
	print "not ok $test\n";
	exit;
    }
    $test++;
}
