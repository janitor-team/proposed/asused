use 5.005;
use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.

# RIPE NCC common configuration
my %PARAM = (
    'INSTALLDIRS'	=> 'vendor',
    'INSTALLSCRIPT'	=> '$(INSTALLVENDORBIN)', # Hack for Perl prior 5.8.1
    'dist'		=> {
	'COMPRESS'	=> 'gzip', 
	    'SUFFIX'	=> '.gz',
	    'CI'	=> 'cvs ci',
	    'RCS_LABEL'	=> 'cvs tag -c -F $(NAME_SYM)-$(VERSION_SYM)',
    },
);

if($] >= 5.005) {
    $PARAM{AUTHOR}		= 'Timur Bakeyev <timur@ripe.net>';
};

WriteMakefile(
    'NAME'	=> 'RipeWhois::FormatMode',
    'VERSION_FROM' => 'FormatMode.pm', # finds $VERSION
    %PARAM
);
