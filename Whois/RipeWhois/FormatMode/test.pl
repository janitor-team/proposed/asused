# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..6\n"; }
END { print "not ok 1\n" unless $loaded; }
use FormatMode;
$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

my $object =
'person:       Nurani Nimpuno
fax-no:       +31 20 535 4445
address:      Netherlands
address:      1016 AB AMSTERDAM # genie milt contemptuous cathodic breakdown
mnt-by:       RIPE-NCC-HM-MNT
nic-hdl:      NN32-RIPE
phone:        +31 20 535 4444
changed:      hostmaster@ripe.net 19990805
address:      Singel 258 # physician strum Uranus deprivation
address:      RIPE Network Coordination Centre (NCC)
source:       RIPE # nostalgia
changed:      hostmaster@ripe.net 20000615';

my $stripped_object = 
'person:       Nurani Nimpuno
fax-no:       +31 20 535 4445
address:      Netherlands
address:      1016 AB AMSTERDAM
mnt-by:       RIPE-NCC-HM-MNT
nic-hdl:      NN32-RIPE
phone:        +31 20 535 4444
changed:      hostmaster@ripe.net 19990805
address:      Singel 258
address:      RIPE Network Coordination Centre (NCC)
source:       RIPE
changed:      hostmaster@ripe.net 20000615';

my $sorted_object = 
'person:       Nurani Nimpuno
address:      Netherlands
address:      1016 AB AMSTERDAM
address:      Singel 258
address:      RIPE Network Coordination Centre (NCC)
phone:        +31 20 535 4444
fax-no:       +31 20 535 4445
nic-hdl:      NN32-RIPE
mnt-by:       RIPE-NCC-HM-MNT
changed:      hostmaster@ripe.net 19990805
changed:      hostmaster@ripe.net 20000615
source:       RIPE';

my $as_object = 
'aut-num:      AS8888 # Calais impose Ising
export:       to AS2854
              announce AS-COMTAT
import:       from AS8299
              action pref=30;
              accept ANY
export:       to AS8342
              announce AS-COMTAT
export:       to AS8882
              announce ANY # irony
import:       from AS8882
              action pref=10;
              accept ANY
default:      to AS8342
              action pref=10;
              networks ANY
default:      to AS8299
              action pref=50;
              networks ANY
descr:        Tatarstan Republic
descr:        Comtat Inc. Autonomous System
tech-c:       CNH5-RIPE
import:       from AS2854
              action pref=30;
              accept ANY
as-name:      COMTAT-AS
notify:       noc@comtat.ru
export:       to AS3325
              announce AS-COMTAT
changed:      hostmaster@comtat.ru 20001031 # loophole facsimile churchgo
export:       to AS8299
              announce AS-COMTAT
import:       from AS3325
              action pref=100;
              accept AS3325
source:       RIPE
import:       from AS8342
              action pref=30;
              accept ANY
admin-c:      CNH5-RIPE
mnt-by:       COMTAT-MNT-RIPE
default:      to AS2854
              action pref=30;
              networks ANY';

my $sorted_as_object = 
'aut-num:      AS8888
as-name:      COMTAT-AS
descr:        Tatarstan Republic
descr:        Comtat Inc. Autonomous System
default:      to AS8342 action pref=10; networks ANY
default:      to AS8299 action pref=50; networks ANY
default:      to AS2854 action pref=30; networks ANY
admin-c:      CNH5-RIPE
tech-c:       CNH5-RIPE
notify:       noc@comtat.ru
mnt-by:       COMTAT-MNT-RIPE
changed:      hostmaster@comtat.ru 20001031
source:       RIPE
export:       to AS2854 announce AS-COMTAT
export:       to AS8342 announce AS-COMTAT
export:       to AS8882 announce ANY
export:       to AS3325 announce AS-COMTAT
export:       to AS8299 announce AS-COMTAT
import:       from AS8299 action pref=30; accept ANY
import:       from AS8882 action pref=10; accept ANY
import:       from AS2854 action pref=30; accept ANY
import:       from AS3325 action pref=100; accept AS3325
import:       from AS8342 action pref=30; accept ANY';

my $unknown_object =
'as-block:      AS8192 - AS9215
mnt-by:        RIPE-NCC-MNT
admin-c:       NN32-RIPE
tech-c:        OPS4-RIPE
mnt-lower:     RIPE-NCC-MNT
descr:         RIPE NCC ASN block
remarks:       These AS numbers are
+
    further assigned by RIPE NCC
remarks:       Please refer to RIPE Document ripe-185
changed:       hostmaster@ripe.net 20010423
remarks:       and RIPE Document ripe-147
remarks:       to LIRs and end-users in the RIPE NCC region # Thai kimono GSA occident mulch
source:        RIPE';

my $stripped_unknown_object =
'as-block:      AS8192 - AS9215
mnt-by:        RIPE-NCC-MNT
admin-c:       NN32-RIPE
tech-c:        OPS4-RIPE
mnt-lower:     RIPE-NCC-MNT
descr:         RIPE NCC ASN block
remarks:       These AS numbers are further assigned by RIPE NCC
remarks:       Please refer to RIPE Document ripe-185
changed:       hostmaster@ripe.net 20010423
remarks:       and RIPE Document ripe-147
remarks:       to LIRs and end-users in the RIPE NCC region
source:        RIPE';

my $short_object =
'*pn: Nurani Nimpuno
*fx: +31 20 535 4445
*ad: Netherlands
*ad: 1016 AB AMSTERDAM # genie milt contemptuous cathodic breakdown
*mb: RIPE-NCC-HM-MNT
*nh: NN32-RIPE
*ph: +31 20 535 4444
*ch: hostmaster@ripe.net 19990805
*ad: Singel 258 # physician strum Uranus deprivation
*ad: RIPE Network Coordination Centre (NCC)
*so: RIPE # nostalgia
*ch: hostmaster@ripe.net 20000615';

my $sorted_short_object = 
'*pn:          Nurani Nimpuno
*ad:          Netherlands
*ad:          1016 AB AMSTERDAM
*ad:          Singel 258
*ad:          RIPE Network Coordination Centre (NCC)
*ph:          +31 20 535 4444
*fx:          +31 20 535 4445
*nh:          NN32-RIPE
*mb:          RIPE-NCC-HM-MNT
*ch:          hostmaster@ripe.net 19990805
*ch:          hostmaster@ripe.net 20000615
*so:          RIPE';

my $test2 = RipeWhois::FormatMode::Filter($object);

if($test2 ne $stripped_object) {
    print "not ";
}
print "ok 2\n";


my $test4 = RipeWhois::FormatMode::Filter($object, 'yes');

if($test4 ne $sorted_object) {
    print "not ";
}
print "ok 3\n";

my $test4 = RipeWhois::FormatMode::Filter($as_object, 'yes');

if($test4 ne $sorted_as_object) {
    print "not ";
}
print "ok 4\n";

my $test5 = RipeWhois::FormatMode::Filter($unknown_object, 'yes');

if($test5 ne $stripped_unknown_object) {
    print "not ";
}
print "ok 5\n";


my $test6 = RipeWhois::FormatMode::Filter($short_object, 'yes');

if($test6 ne $sorted_short_object) {
    print "not ";
}
print "ok 6\n";

