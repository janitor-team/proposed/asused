use Config;
use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.

# RIPE NCC common configuration
my %PARAM = (
    'INSTALLDIRS'	=> 'vendor',
    'INSTALLSCRIPT'	=> '$(INSTALLVENDORBIN)', # Hack for Perl prior 5.8.1
    'dist'		=> {
	'COMPRESS'	=> 'gzip', 
	    'SUFFIX'	=> '.gz',
	    'CI'	=> 'cvs ci',
	    'RCS_LABEL'	=> 'cvs tag -c -F $(NAME_SYM)-$(VERSION_SYM)',
    },
);

if($] >= 5.005) {
    $PARAM{AUTHOR}		= 'Timur Bakeyev <timur@ripe.net>';
}

my $script_name = 'asused3';
my $private = (-f '/ncc/registries/zz.example') ? 1 : 0;

unless($private) {
    $script_name = 'asused';
    $PARAM{INSTALLDIRS} = 'site';
    $PARAM{INSTALLSCRIPT} = '$(INSTALLSITEBIN)';
}

WriteMakefile(
    'NAME'	=> 'asused',
    'PREREQ_PM'	=> {
	'IO::Socket'	=> '1.20', # We would like to have quiet modern IO::Socket
    },
    'VERSION_FROM'=> 'asused.PL', # finds $VERSION
    'EXE_FILES'	=> [ $script_name ],
    'PL_FILES'	=> { 'asused.PL' => $script_name },
    'clean'	=> { 'FILES' => $script_name },
    %PARAM
);   

# Ugly hack to deal with Perl prior 5.00503
sub MY::processPL {
    my($self) = shift;
    return "" unless $self->{PL_FILES};
    my(@m, $plfile);
    foreach $plfile (sort keys %{$self->{PL_FILES}}) {
        my $list = ref($self->{PL_FILES}->{$plfile})
		? $self->{PL_FILES}->{$plfile}
		: [$self->{PL_FILES}->{$plfile}];
	my $target;
	foreach $target (@$list) {
	push @m, "
all :: $target
	$self->{NOECHO}\$(NOOP)

$target :: $plfile
	\$(PERL) -I\$(INST_ARCHLIB) -I\$(INST_LIB) -I\$(PERL_ARCHLIB) -I\$(PERL_LIB) $plfile $target
";
	}
    }
    join "", @m;
}
