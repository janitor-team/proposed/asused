=head1 NAME 

B<asused> - summaries  address  space used and   according to  the RIPE DB and REG. 

=head1 SYNOPSIS

B<asused>  [B<--all>]  [B<--aw> | B<--approval>] [B<--overlap>] [B<--status> | B<--assign>[B<--pipa>] ]  I<regid>

B<asused> [B<--all>] [B<--aw> | [B<--approval>]  [B<--overlap>]  [B<--status> | B<--assign> [B<--pipa>] ] [B<--regid> I<regid>] (I<prefix> ...)

B<asused>  [--B<<options>>]  I<regid> | prefix


=head1 DESCRIPTION

B<asused> is a tool to summaries  address space is  registered  in the RIPE database.  For each allocated inetnum object a summary of  used and free address space   is printed.  A grant total summary for all prefixes is also provided. If there are no errors in locating allocations and and assignments under an allocation. In in the total % are calculated on total allocations.   

where:
 
I<regid> is name of registry as in registry database. 

I<prefix>  is allocation as in whois  database. e.g. a.b.c/16 . Prefix  is queried  whois DB to  find all Less specific matches   inetnum with I<na> as netname is interpreted as allocation  and summerize the allocation. 

=head1 OPTIONS

=over 4

=item B<--assign> print all assignments under allocations, size, date  and net name .  Summary of assignments. And free address space. Summary of status attribute.

=item B<--all> this same as three options  B<--overlap> B<--status> B<--aw>.

=item B<--approval>  Check the all networks in allocations  are  valid or not. Print valid and invalid networks. B<--aw> only print invalid networks. 

=item B<--aw> Check the all networks in allocations  are  valid or not. Print only Invalid networks. B<--approval> will print valid and invalid.

=item B<--contacts>   print admin-c/tech-c for all inetnums

=item B<--help>  Shows the usage of B<asused>.

=item B<--overlap> print the details of overlapping assignments. Summary of overlap per assignment is printed at the end. 

=item B<--reg> I<regid>. regid to locate  the allocation. Netname of allocation is same as regid,  translating the first   '.' to '-' IN upper case. If not specified uses i2r to get regid.

=item B<--status> show assignments under allocations with wrong status attribute other than ASSIGNED PA or PI.  B<MISSING> for missing status. size and net name.  Summary of assignments.  No of Assignment(s) ,  No Assignments with status ASSIGNED PA or ASSIGNED PI,   No Assignments with status B<ASSIGNED PI>,No Assignments, No Assignments with Missing status attribute,No Assignments with other values status attribute, none of other .  Warnings on assignments.

=head1 OTHER OPTIONS

=over 4 

=item B<as3-option>

=over 8

=item B<--port> I<port>  query another whois server on the I<port>. Default is 43.

=item B<--host> I<whoishost> query another whois server. Default is whois.ripe.net. Can query only to RIPE whois server. Server version tested '2.1'

=item B<--na> I<netname> show event table for approval of I<netname>. Used with B<--aw> or B<--approval>

=item B<-pipa> additional flag to the B<--assign> option to print allocation status in output. PA is for ALLOCATED
PA, PI - ALLOCATED PI, UN for UNSPECIFED, B<--> is for missed B<status> field.

=item B<--version> print version of asused.


=head1 DIAGNOSTICS

=over 4

=item B<No allocations from reg> Could  not locate any allocations in use from registry.

=item B<No allocations from DB> No inetnum object was recognized as allocation in the RIPE Database.  

=item B< % No entries found for the selected source(s).> No assignment(s)  found  under the allocation. May be a new allocation or allocation which has no objects in RIPE DB. Not sure to treat as 100 % free.

=item B<ERROR MISMATCH Netname regid> RIPE DB  B<Netame> attribute  has mismatch with I<na> or regid. 

=item B<ERROR STATUS> May be incorrect  status attribute in the RIPE DB, Valid status ALLOCATED (PA|PI|UNSPECIFIED)

=item B<ERROR mnt-by>Invalid <mnt-by> attribute in RIPE DB  expecting  RIPE-NCC-MNT|RIPE-NCC-HM-MNT|RIPE-NCC-HM-PI-MNT  

=item B<Inconsistent with I<inetnum>> Encountered two inetnum  objects with same logical start and end. Logical interpretation of inetnum  a.b.c.0 =>  a.b.c.0 - a.b.c.255; a.b.c.0 - a.b.d.0 =>  a.b.c.0 - a.b.d.255 

=item B<Classfull notation> Intrepreted as class full object.  Check if no of address are counted correctly. 

=item B<Multiple inetnum> When tried to locate an allocation inetnum object for I<regid> I<prefix>  from RIPE DB with option -L -F -T I<prefix> returned multiple objects. Every prefix can have only one allocation object.  

=item B<NOT Counted  Assignment> If the assignment is outside the boundaries of allocation. It was returned by -M query RIPE whois server.

=item B<Invalid inetnum> Logical interpretation of inetnum fails.  Probably  can not  identify start,  end of end < start. 

=item B<Assignments has Invalid Date> one of the dates of inetnum in the changed attribute is invalid. 


=head1 REQUIRES

Perl 5.00404 or later.   Connection to RIPE  whois server V 2.1, Perl Modules  Socket Getopt::Long, regread, ipv4pack

=head1 BUGS

Could give incorrect summary if logical interpretation of Inetnum is not exact  a.b.c.0 - a.b.c.0 is intended to be an assignment of size 1 IP No.  

B<Out of memory!> Noticed that when  the no of assignments are very large, like 5000+ per allocation default data segment  size of BSDI 3.1 is not enough.  Increasing to 64 M Bytes may help.  in bash shell B<ulimit -d 65536> eg. de.schlund

B<IANA Delegated  blocks smaller than /8> If the allocations are outside 192,193,194,195,212 /8 and LIR allocation is same inetnum as in /ncc/ip-reg/delegations asused2 will return I<No allocations in RIPE DB>. eg. de.callisto

=head1 SEE ALSO 

L<whois>   L<reg>   L<i2r> L<regread>  L<Reg::Asused> L<Reg::Approved> L<Net::RIPEWhois> 

=head1 AUTHOR

Antony Antony <antony@ripe.net> . RIPE NCC software group. <softies@ripe.net>

=cut


